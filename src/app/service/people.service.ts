import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';
import { StudentService } from './student-service';

@Injectable({
  providedIn: 'root'
})
export class PeopleService extends StudentService {

  constructor(private http: HttpClient) { 
    super();
  }
  getStudents():Observable<Student[]>{
    return this.http.get<Student[]>('https://se331.s3-ap-southeast-1.amazonaws.com/people.json')
  }
}
