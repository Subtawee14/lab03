import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { HttpClientModule } from '@angular/common/http'; 
import { PeopleService } from './service/people.service';


@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  
  imports:[
  BrowserModule,
  HttpClientModule,
  FormsModule
  ],
  providers: [
    { provide : StudentService, useClass: PeopleService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
